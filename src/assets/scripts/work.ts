const works = [
  {
    name: 'ポートフォリオサイト',
    src: require('../images/portfolio.png'),
    alt: '本ポートフォリオサイトへのリンク',
    linkExternal: 'https://izumeeeel.com',
    description:
      '本サイトのデザインから実装まで。CLIなどを使わず、なるべく手作りで実装しました。',
    language:
      'Vue.js(Vue 3), webpack, TypeScript, SCSS, GitLab CI/CD, S3, MicroCMS',
    timeSpent: '２ヶ月（技術選定・デザイン・実装含む）'
  },
  {
    name: 'コーヒーショップ向けECサイト',
    src: require('../images/works-unicom-seed.jpg'),
    alt: 'コーヒーショップ向けECサイトへのリンク',
    linkExternal: 'https://coffeebigisland.unicom.social/',
    description:
      'Vue.jsを用いたECサイトのフロントエンドの開発に携わりました。デザインの詳細設計にも関わりました。2020/05にリリースしました。',
    language: 'Vue.js, Nuxt.js, Vuetify',
    timeSpent: '５ヶ月（チームでの開発に途中から参画）'
  },
  {
    name: 'Daily Scheduler',
    src: require('../images/daily-scheduler.png'),
    alt: 'Daily Schedulerへのリンク',
    linkExternal: 'https://izumi-daily-scheduler.web.app/',
    description:
      'その日の予定と実際にやったことを併記できるWEBアプリです。「あれ、あの時間なにしてたっけ？」といった無駄な時間を無くせます。',
    language: 'Vue.js, Vuetify, Firebase(Firestore)',
    timeSpent: '20時間'
  }
]
export default works
