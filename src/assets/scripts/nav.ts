import { Nav } from '../../models/Footer'

export const navs: Nav = {
  about: {
    name: 'Home',
    src: require('../../assets/images/icon-about.svg'),
    alt: 'home',
    linkTo: '/'
  },
  works: {
    name: 'Works',
    src: require('../../assets/images/icon-works.svg'),
    alt: 'works',
    linkTo: '/works'
  },
  blog: {
    name: 'Blog',
    src: require('../../assets/images/icon-blog.svg'),
    alt: 'blog',
    linkTo: '/blog'
  }
}
