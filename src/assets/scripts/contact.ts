import { NavItem } from '../../models/Footer'

const contacts: { [key: string]: NavItem } = {
  gmail: {
    name: 'Gmail',
    src: require('../../assets/images/icon-gmail.svg'),
    alt: 'Gmailのアイコン',
    linkTo: 'javascript:void(0)', //画面遷移させない
    text: 'メールアドレスをコピーする'
  },
  gitlab: {
    name: 'GitLab',
    src: require('../../assets/images/icon-gitlab.svg'),
    alt: 'GitLabのアイコン',
    linkTo: 'https://gitlab.com/Izumeeeel',
    text: 'GitLabを見る'
  },
  wantedly: {
    name: 'Wantedly',
    src: require('../../assets/images/icon-wantedly.svg'),
    alt: 'Wantedlyのアイコン',
    linkTo:
      'https://www.wantedly.com/secret_profiles/N1JmQPZt5vKXN9EPjar4WGxxwJb9QDN9',
    text: 'Wantedlyを見る'
  }
}
export default contacts
