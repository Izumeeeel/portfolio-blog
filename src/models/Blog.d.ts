export interface Blog {
  data: {
    contents: [
      {
        id: string
      }
    ]
  }
}
