export interface NavItem {
  name: string
  src: string
  alt: string
  linkTo: string
}

export interface Nav {
  about: NavItem
  works: NavItem
  blog: NavItem
  contact: NavItem
}
