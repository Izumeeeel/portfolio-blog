export interface NavItem {
  name: string
  src: string
  alt: string
  linkTo?: string
  text?: string
}

export interface Nav {
  about: NavItem
  works: NavItem
  blog: NavItem
}
