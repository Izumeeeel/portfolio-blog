export interface Language {
  name: string
  level: number
  color: string
  notation: string
}
export interface EventDateCount {
  [date: string]: number
}

export interface GitlabEvent {
  action_name: string
  author: {
    id: number
    name: string
    username: string
    state: string
    avatar_url: string
  }
  author_id: number
  author_username: string
  created_at: Date
  project_id: number
  push_data: {
    commit_count: number
    action: string
    ref_type: string
    commit_from: string
    commit_to: null
  }
  target_id: null
  target_iid: null
  target_title: null
  target_type: null
}
