import { RouteRecordRaw, createRouter, createWebHistory } from 'vue-router'

import Home from '../pages/Home.vue'
import Works from '../pages/Works.vue'
import Blog from '../pages/blog/index.vue'
import Article from '../pages/blog/_id.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/works',
    name: 'Works',
    component: Works
  },
  {
    path: '/blog',
    name: 'Blog',
    component: Blog
  },
  {
    path: '/blog/:id',
    name: 'Blog',
    component: Article
  }
]

const router = createRouter({
  history: createWebHistory('/'),
  routes
})

export default router
