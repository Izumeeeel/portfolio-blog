<h2>詰め込みたいもの</h2>

**デザイン**
* [ ]  Atomic Design
* [ ]  Scroll Animation
* [ ]  Responsive対応
* [ ]  Darkmode対応

**機能・技術**
* [x]  Vue
* [x]  Composition API
* [x]  SASS
* [x]  TypeScript
* [x]  JEST
* [x]  Gulp Webpack
* [x]  MicroCMS
* [x]  AWS
* [ ]  アクセス分析？
* [x]  Circle CI
---
* [ ]  SVGやCanvasなどを利用したアニメーション
* [ ]  three.js
* [x]  BEM